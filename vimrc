set nocompatible
"set number
"set autoindent
set hlsearch
set relativenumber
set runtimepath^=~/.vim/bundle/ctrlp.vim
"inoremap jk <ESC>
"inoremap <esc> <nop>
inoremap ( ()<C-G>U<Left>
inoremap { {}<C-G>U<Left>
let mapleader = ","
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
"filetype plugin indent on
syntax on
set encoding=utf-8
execute pathogen#infect()
" should markdown preview get shown automatically upon opening markdown buffer
let g:livedown_autorun = 0

" should the browser window pop-up upon previewing
let g:livedown_open = 1 

" the port on which Livedown server will run
let g:livedown_port = 1337

map gm :call LivedownPreview()<CR>
